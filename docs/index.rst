.. Rosenfeld Lab Pipeline documentation master file, created by
   sphinx-quickstart on Thu Aug  6 14:08:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :caption:Documentation
   :maxdepth: 1

====================================
Rosenfeld Lab Pipeline Documentation
====================================

------------
Version News 
------------

v0.0.2 
17 Aug 2020

- Fix a bug in read selection step.
- Update documentation.
- Other improvments.

-----------------------


v0.0.1.9000
10 Aug 2020 

- Added documentation. 
- Initiation of this new pipeline.



.. toctree::
   :caption: For New Users Only
   :name: new_user
   :maxdepth: 2

   subsession/new_user



.. toctree::
   :caption: Quick Start
   :name: new_user
   :maxdepth: 1

   subsession/quick_start




.. toctree::
   :caption: Usage
   :name: usage
   :maxdepth: 1

   subsession/usage



.. toctree::
   :caption: Workflow Routes
   :name: routes
   :maxdepth: 1

   subsession/routes


.. toctree::
   :caption: Software Version 
   :name: software
   :maxdepth: 2

   subsession/software_version.rst


.. toctree::
   :caption: Parameters  
   :name: params
   :maxdepth: 2

   subsession/params.rst


.. toctree::
   :caption: Reference 
   :name: ref
   :maxdepth: 1

   subsession/ref


.. toctree::
   :caption: Best Practices
   :name: bp
   :maxdepth: 1

   subsession/best_practices

