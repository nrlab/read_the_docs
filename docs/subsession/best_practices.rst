---------------------------
Use an old version pipline
---------------------------

Some data was analyzed three years ago, during that time version 1.1.4 was used.
Now the rosenfeld_lab_pipeline has evolved into version 2.0.0 and you want to reproduce 
the results generated three years ago. You should 
download the early version pipeline using --branch 1.1.4 :


.. code:: bash

    git clone --branch 1.1.4 git@bitbucket.org:haichaowang/rosenfeld_lab_pipeline.git


In this case the following two mistakes should be avoided:

.. code:: bash

    # This will download the latest version.

    git clone git@bitbucket.org:haichaowang/rosenfeld_lab_pipeline.git
    
    # This will download v1.1.5. (if it exists, otherwise errors will show). 
    
    git clone --branch 1.1.5 git@bitbucket.org:haichaowang/rosenfeld_lab_pipeline.git


To be short, always set the --branch to what you need.

--------------------------
 Prepare a manuscript
--------------------------

After 3 years hardwork, you are writing your paper manuscript. In the methodology part,
the version nubmer of all analysis tools used in the study will be needed.

Now you should go back to the Software Version session in this documentaion, choose the 
version number, then everyting information will be ready for you.