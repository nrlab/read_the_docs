--------------------------
Ask IT for cluster access
--------------------------

- Email IT and ask them for access to cluster.


Email template:

Dear IT,

I am a new student/staff based in Nitzan lab, I will use cluster1 for data analysis.
Could you help grant me access to the cluster?

Best,
Your Name

----------------------------
Initiate anaconda
----------------------------
Run the following codes line-by-line on cluster (this is one-off, you don't need to run it again in future):

/home/nrlab/tools/anaconda3/bin/conda init bash  

source /home/${USER}/.bashrc  

conda config --set auto_activate_base false  



-----------------------------
Install Dependencies/software
-----------------------------