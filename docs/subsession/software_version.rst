
-----------------------
 Dependency 
-----------------------

The pipine was run in a conda environment:

rosenfeld_lab_pipeline

Dependencies pre-installed in this environment:

  - python=2.7
  - r-base=3.5.1
  - r-essentials
  - xlrd
  - pysam=
  - 

--------------------------------------
Software 
--------------------------------------

Data download: 

.. csv-table:: 
   :file: ../software_version_csv/data_download_version.csv
   :widths: 20, 20, 60
   :header-rows: 1




NRlab DNAseq Pipeline:

.. csv-table:: 
   :file: ../software_version_csv/nrlab_pipeline_version.csv
   :widths: 20, 20, 60
   :header-rows: 1


Connor:

.. csv-table:: 
   :file: ../software_version_csv/connor_version.csv
   :widths: 20, 20, 60
   :header-rows: 1


tMAD:

ichorCNA:

WALDO:

INVAR:





