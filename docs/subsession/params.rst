------------------
Default settings
------------------

Here are the default setting in  /config/config.yaml file in the pipeline code.

Modification of these parameters before running the pipeline might be required.
Please store your own config.yaml file carefully.

.. code:: yaml

   clarity-tools

