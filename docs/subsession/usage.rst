
--------------------------------------
Step 0: Set-up
--------------------------------------


Login to the cluster:


.. code:: bash

     ssh clust1-headnode.cri.camres.org


Move into your user directory on the cluster:

.. code:: bash

      cd /scratcha/nrlab/userID 
      
Create a new analysis directory:

.. code:: bash
    
     mkdir SLX-*****_project_name
    
Move into the analysis directory:

.. code:: bash

    cd SLX-*****_project_name
    
    

This is where we can clone the pipeline.

|

--------------------------------------
Step 1: Obtain a copy of this workflow
--------------------------------------
 

.. code:: bash

    git clone git@bitbucket.org:nrlab/rosenfeld_lab_pipeline.git


Ths clones the most recent version of the pipeline to directory 'rosenfeld_lab_pipeline'.

|

------------------------------
Step 2: Configure workflow
------------------------------


cd to the pipeline folder

.. code:: bash

    cd rosenfeld_lab_pipeline


Modify the config files in ./config/config.yaml

Important: 

- YAML file requires VERY strict identation. Use blank space for indentation. Don't mix-up blank space and tab (Yes, some people use Space, and some use Tab). Keep the identation consistent with default ones. 


- NEVER use Microsoft Word for editing. I recommend Visual Code for MacOS, and Notepad++ for Windows. But in most cases, you just need to edit config.yaml on cluster directly using vim or nano.


Configuration in practice should follow the indentation and syntax requiremnt of YAML file:


.. code-block:: yaml
  :linenos:

  data_download:
    clarity_tools: /home/nrlab/tools/clarity-tools.jar 
    slxid: 
      - SLX-17338
      - SLX-13900
      - SLX-10340 


Could you spot the bug(s) in the following config settings?

.. code-block:: yaml
  :linenos:
  :emphasize-lines: 6

  data_download:
    clarity_tools: /home/nrlab/tools/clarity-tools.jar 
    slxid: 
      - SLX-17338
      - SLX-13900
      -SLX-10340 

(Answer: missing one space between '-' and 'SLX-10340' in line 6, the pipeline will stop with an error message.)

|

------------------------------------------------------------
Edit the config file using a text editor - Nano.
------------------------------------------------------------


From withinh the pipeline, edit the config.yaml with Nano text editor.

.. code:: bash

    nano config/config.yaml
    
    
Input the values relevant to your route and analysis. Follow indentation of the config.yaml file.

'Ctrl X' to exit and save.


|

------------------------------
Step 3: Execute workflow 
------------------------------


Launch screen before running pipeline to avoid lost connection issues. 
This is essential if you want to back-up BAMs.

.. code:: bash

     screen -S your_session_name
     
If connection was lost, then the screen session can be re-opened with:

.. code:: bash

     screen -r your_session_name

Run the pipeline.

.. code:: bash

    ./run.sh -r A1
    
OR

.. code:: bash

    ./run.sh -r A1 -a WES/Project_Type_Initials
  
  
Option [-r] is used to indicate a route of the pipeline.

For route options, please refer to the [Available routes] section in this documentation. 

Alternatively, enter command './run.sh -h' to reveal available routes from within the pipeline.




Option [-a] is used to indicate the directory where the USER wants to back-up BAM files.

To do so, the USER must manually create a target directory in:

.. code:: bash

    /mnt/nas-data/nrlab/group_folders/SharedData/

**IMPORTANT** The back-up directory **must** follow the **'Project_DataType_Initials'** format.

For example, **'sWGS/MelR_plasma_PM_DG'** follows the established **'Project_DataType_Initials'** format.

For example, to create WES/Project_DataType_Initials directory:

.. code:: bash 
      
      mkdir -p /mnt/nas-data/nrlab/group_folders/SharedData/WES/Project_Type_Initials


If the USER is unsure how to name their back-up directory, pre-existing directories can be viewed as examples.

.. code:: bash

       ls -l  /mnt/nas-data/nrlab/group_folders/SharedData/sWGS 
       


Option [-a] can be omitted if no BAM backup is required.

.. code:: bash

    ./run.sh -r A1

However, [-r] must always be followed by a **ROUTE** number or the pipeline will not launch.




------------------------------
Step 4: Investigate results 
------------------------------


.. code:: bash

    cd results
    
The [results] directory will contain outputs from the selected pipeline ROUTE once the pipeline run has finished.

    
  

