------------------
Available Routes
------------------

The routes can also be visualized within the pipeline by using the [-h] option.



.. code:: bash

      cd rosenfeld_lab_pipeline
 
 
.. code:: bash

      ./run.sh -h 



------------------
A Routes
------------------

Routes starting with 'A' execute the pipeline from the absolute beginning by downloading the data.


A Routes:

route A1: data download + NRlab Alignment + tMAD and ichorCNA + Size-selection + Size-selected t-MAD and ichorCNA

route A2: data download + NRlab Alignment + tMAD and ichorCNA

route A3: data download + NRlab Alignment + Size-selection 

route A4: data download + NRlab Alignment 

route A5: data download







------------------
B Routes
------------------


Routes starting with 'B' execute the pipeline with pre-existing FASTQs. 
The user must provide the absolute path to FASTQs in [config.yaml] [downloaded_fq_path:] field. 
For example:

.. code:: bash

      downloaded_fq_path: /scratcha/user/project/fastqs

Note no forward slash after directory!


B Routes:

route B1: Pre-existing FQs + NRlab Alignment + tMAD and ichorCNA + Size-selection + Size-selected tMAD and ichorCNA

route B2: Pre-existing FQs + NRlab Alignment + tMAD and ichorCNA

route B3: Pre-existing FQs + NRlab Alignment + Size-selection 

route B4: Pre-existing FQs + NRlab Alignment 








------------------
C Routes
------------------


Routes starting with 'C' execute the pipeline with pre-existing BAMs. 
The user must provide the absolute path to BAMs in [config.yaml] file's [path_to_BAMs:] field. 
For example:

.. code:: bash

      path_to_BAMs: /scratcha/user/project/BAM_directory

Note no forward slash after directory!


C Routes:

route C1: Pre-existing BAMs + tMAD and ichorCNA + Size-selection + Size-selected tMAD and ichorCNA

route C2: Pre-existing BAMs + tMAD 

route C3: Pre-existing BAMs + Size-selection 

|

------------------
Clean-up Route
------------------

Route 0 is the CLEAN-UP route meant for 'A' routes. 
It removes all BAM and BAM.BAI files from the [results/nrlab_dnaseq_pipeline/bam_files] directory.
It should be launched at the end of analysis when important files and outputs have been saved elsewhere.



.. code:: bash

      ./run.sh -r 0


'B' and 'C' routes do not store pre-existing FQ and BAM files, therefore there is no need to remove them.


